
import csstype.Display.Companion.block
import csstype.NamedColor
import csstype.Position.Companion.absolute
import csstype.px
import emotion.react.css
import react.FC
import react.Props
import react.dom.html.ReactHTML.button
import react.dom.html.ReactHTML.div
import react.dom.html.ReactHTML.h3

external interface VideoPlayerProps: Props {
    var video: Video
    var onWatchedButtonPress: (Video) -> Unit
    var unwatchedVideo: Boolean
}

val VideoPlayer = FC<VideoPlayerProps> { props ->

    div {
        css {
            position = absolute
            top = 10.px
            right = 10.px
        }
        h3 { +"Рутьюб 2" }
        button {
            css {
                display = block
                backgroundColor = if (props.unwatchedVideo) NamedColor.lightgreen else NamedColor.red
            }
            onClick = {
                props.onWatchedButtonPress(props.video)
            }
            if (props.unwatchedVideo)
                +"Mark as watched"
            else
                +"Mark as unwatched"
        }
        div {
            css {
                position = absolute
                top = 10.px
                right = 10.px
            }
            EmailShareButton {
                url = props.video.videoUrl
                EmailIcon {
                    size = 32
                    round = true
                }
            }
            TelegramShareButton {
                url = props.video.videoUrl
                TelegramIcon {
                    size = 32
                    round = true
                }
            }
        }
        ReactPlayer {
            url = props.video.videoUrl
            controls = true
        }
    }
}