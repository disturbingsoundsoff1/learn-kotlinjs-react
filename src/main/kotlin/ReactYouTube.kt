@file:JsModule("react-player")
@file:JsNonModule

import react.ComponentClass
import react.Props

@JsName("default")
external val ReactPlayer: ComponentClass<ReactYouTubeProps>

external interface ReactYouTubeProps : Props {
    var url: String
    var controls: Boolean
}