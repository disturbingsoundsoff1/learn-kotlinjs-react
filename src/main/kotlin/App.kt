
import kotlinx.browser.document
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import react.FC
import react.Props
import react.dom.html.ReactHTML.div
import react.dom.html.ReactHTML.h1
import react.dom.html.ReactHTML.h3
import react.useEffectOnce
import react.useState


val mainScope = MainScope()

val App = FC<Props> {
    var currentVideo: Video? by useState(null)

    var unwatchedVideos: List<Video> by useState(emptyList())
    var watchedVideos: List<Video> by useState (emptyList())

    useEffectOnce {
        mainScope.launch {
            unwatchedVideos = fetchVideos()
        }
    }

    document.bgColor = "#161320"
    document.fgColor = "#c0cce3"

    h1 {
        +"Рутьюб 2"
    }
    div {
        h3 {
            +"Videos to watch"
        }
        VideoList {
            videos = unwatchedVideos
            selectedVideo = currentVideo
            onSelectVideo = {video -> currentVideo = video }
        }
        h3 {
            +"Videos watched"
        }
        VideoList {
            videos = watchedVideos
            selectedVideo = currentVideo
            onSelectVideo = {video -> currentVideo = video }
        }
    }

    currentVideo?.let { curr ->
        VideoPlayer {
            video = curr
            unwatchedVideo = curr in unwatchedVideos
            onWatchedButtonPress = {
                if (video in unwatchedVideos) {
                    unwatchedVideos -= video
                    watchedVideos += video
                } else {
                    watchedVideos -= video
                    unwatchedVideos += video
                }
            }
        }
    }

}